# Changelog
All notable changes to this project will be documented in this file.
The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/)

## [3.0.0 RC-2] - 2019-06-11
### Changed
* Minor bugfixes and refactoring.
* Updates to JS es6 syntax on node backend.
* Fixed bug where if music were still playing when the app is closed it left the browser window open an inaccessible.

## [3.0.0 RC-1] - 2019-06-10
### Changed
* electron [1.4.12] -> [1.8.8]
* electron-packager(removed)
* electron-installer-debian(removed)
* electron-builder(added)
    * [electron-builder documentation](https://www.electron.build/configuration/linux)
* electron-installer-appimage(added) [1.0.1]
    * Added for linux distribution via AppImage
* node(project level dep added) [12.4.0]
* npm(project level dep added) [6.9.0]