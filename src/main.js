'use strict';
/*
Imports
 */
const constants = require('./modules/constants');
const util = require('./modules/window-util.js');
const Electron = require('electron');
const IpcEvents = require('electron').ipcMain;
const Path = require('path');
const App = Electron.app;
const Menu = Electron.Menu;
const MenuItem = Electron.MenuItem;
/*
Constants
 */
const appDataBasePath = App.getPath('appData');
const settings = require('./modules/settings-util.js')(appDataBasePath);
const menubar = require('menubar');
/*
Initialization
 */
const mb = menubar(util.getMenubarConfig());

function buildContextMenu() {
    const showHideLabel = constants.labels.show + (!settings.getAutoHide()
        ? constants.labels.hide + '(' + settings.getShowHideWindowKey() + ')'
        : '');
    const imagePath = Path.join(__dirname, 'resources/images/');
    const separatorConfig = {type: 'separator'};
    const contextMenu = new Menu();
    const menuBarRef = mb;
    contextMenu.append(new MenuItem({
        label: showHideLabel,
        click: (menuItem, bounds, keyboardEvent) => {
            return util.showWindow(menuItem, bounds, keyboardEvent, menuBarRef);
        }
    }));
    const showMenuIcons = settings.getShowMenuIcons();
    if (settings.getShowMenuPlayPause()) {
        contextMenu.append(new MenuItem(separatorConfig));
        contextMenu.append(new MenuItem({
            label: constants.labels.playPause,
            icon: (showMenuIcons ? imagePath + 'pause_play.png' : null),
            click: util.playPause
        }));
    }
    if (settings.getShowMenuNext()) {
        contextMenu.append(new MenuItem(separatorConfig));
        contextMenu.append(new MenuItem({
            label: constants.labels.nextTrack,
            icon: (showMenuIcons ? imagePath + 'next_track.png' : null),
            click: util.playNextTrack
        }));
    }
    if (settings.getShowMenuPrevious()) {
        contextMenu.append(new MenuItem(separatorConfig));
        contextMenu.append(new MenuItem({
            label: constants.labels.previousTrack,
            icon: (showMenuIcons ? imagePath + 'prev_track.png' : null),
            click: util.playPreviousTrack
        }));
    }
    contextMenu.append(new MenuItem(separatorConfig));
    contextMenu.append(new MenuItem({
        label: constants.labels.settings,
        click: util.showSettings
    }));
    contextMenu.append(new MenuItem({
        label: constants.labels.quit, click: () => {
            let window = mb.window;
            App.quit();
            setTimeout(() => {
                if(window != null && !window.isDestroyed()){
                    console.log("Music was still playing, had to force close the window.");
                    window.destroy();
                }
            });
        }
    }));
    return contextMenu;
}

mb.on('ready', function ready() {
    util.setupMediaKeyEvents(mb);
    const tray = mb.tray;
    tray.setContextMenu(buildContextMenu());
    // mb.window.webContents.openDevTools()
});

IpcEvents.on(constants.events.closeSettings, () => {
    util.closeSettings();
    util.setupMediaKeyEvents(mb);
});

IpcEvents.on(constants.events.getAppVersion, (event) => {
    console.log(App.getVersion());
    event.sender.send(constants.events.setAppVersion, App.getVersion());
});

IpcEvents.on(constants.events.getAppDataPath, (event) => {
    console.log(appDataBasePath);
    event.sender.send(constants.events.setAppDataPath, appDataBasePath);
});