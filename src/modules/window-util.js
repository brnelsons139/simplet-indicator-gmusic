'use strict';
const Electron = require('electron');
const BrowserWindow = Electron.BrowserWindow;
const Positioner = require('electron-positioner');
const GlobalShortcut = Electron.globalShortcut;
const Path = require('path');
const settingsModule = require('./settings-util.js')(Electron.app.getPath("appData"));
const musicServices = require('./settings.js').SETTINGS.PLAYER_SETTINGS.VALUES.MUSIC_SERVICE.OPTIONS;
let settingsWindow;
let cachedBounds;
let menuBarRef;

function showWindow(menuItem, bounds, keyboardEvent, mb) {
    if (keyboardEvent != null && (keyboardEvent.altKey || keyboardEvent.shiftKey || keyboardEvent.ctrlKey || keyboardEvent.metaKey)) return mb.hideWindow();
    if (mb.window && mb.window.isVisible()) return mb.hideWindow();
    cachedBounds = bounds || cachedBounds;
    let bool = mb.showWindow(cachedBounds);
    setTimeout(function () {
        mb.window.focus();
    }, 500);
    return bool;
}

function playNextTrack(musicService) {
    menuBarRef.window.webContents.executeJavaScript(musicService.JS_NEXT);
}

function playPreviousTrack(musicService) {
    menuBarRef.window.webContents.executeJavaScript(musicService.JS_PREV);
}

function playPause(musicService) {
    menuBarRef.window.webContents.executeJavaScript(musicService.JS_PLAY_PAUSE);
}

function getActiveMusicService(musicServiceUrl, musicServiceFound) {
    for (const musicService in musicServices) {
        if (musicServices.hasOwnProperty(musicService)) {
            const service = musicServices[musicService];
            if (service['VALUE'] === musicServiceUrl) {
                musicServiceFound = service;
            }
        }
    }
    return musicServiceFound;
}

module.exports = {
    showSettings: () => {
        if (settingsWindow == null) {
            const options = {
                height: 700,
                width: 900,
                frame: true,
                darkTheme: true,
                transparent: true,
                resizable: true,
                webPreferences: {
                    nodeIntegration: true
                }
            };
            settingsWindow = new BrowserWindow(options);
            settingsWindow.setMenu(null);
            // settingsWindow.webContents.openDevTools()
            const positioner = new Positioner(settingsWindow);

            positioner.move('center');
            settingsWindow.loadURL('file://' + Path.join(__dirname, '../settings-bootstrap.html'))
        } else if (settingsWindow.isVisible()) {
            settingsWindow.hide();
        } else {
            settingsWindow.show();
        }
    },

    closeSettings: () => {
        if (settingsWindow != null) {
            settingsWindow.close();
            settingsWindow = null;
        }
    },

    showWindow: (menuItem, bounds, keyboardEvent, mb) => {
        showWindow(menuItem, bounds, keyboardEvent, mb);
    },

    playNextTrack: () => {
        playNextTrack(getActiveMusicService(settingsModule.getMusicService()))
    },
    playPreviousTrack: () => {
        playPreviousTrack(getActiveMusicService(settingsModule.getMusicService()))
    },
    playPause: () => {
        playPause(getActiveMusicService(settingsModule.getMusicService()))
    },

    setupMediaKeyEvents: (mb) => {
        menuBarRef = mb;
        // old way mb.window.webContents.sendInputEvent({type: 'keydown', keyCode: 'Right'});
        mb.window.setAlwaysOnTop(settingsModule.getAlwaysOnTop());
        mb.window.on('blur', function () {
            if (settingsModule.getAutoHide()) {
                mb.window.hide();
            }
        });

        let musicServiceFound = getActiveMusicService(settingsModule.getMusicService());

        GlobalShortcut.unregisterAll();
        GlobalShortcut.register(settingsModule.getMediaNextKey(), function () {
            playNextTrack(musicServiceFound);
        });

        GlobalShortcut.register(settingsModule.getMediaPreviousKey(), function () {
            playPreviousTrack(musicServiceFound);
        });

        GlobalShortcut.register(settingsModule.getMediaPlayPauseKey(), function () {
            playPause(musicServiceFound);
        });

        //show window
        GlobalShortcut.register(settingsModule.getShowHideWindowKey(), function () {
            showWindow(null, cachedBounds, mb)
        })
    },
    getMenubarConfig: () => {
        return {
            dir: __dirname,
            icon: settingsModule.getTrayIconConfiguration(),
            height: parseInt(settingsModule.getWindowHeight()),
            width: parseInt(settingsModule.getWindowWidth()),
            showOnAllWorkspaces: false,
            tooltip: 'GMusic',
            transparent: "true",
            preloadWindow: "true",
            index: settingsModule.getMusicService(),
            windowPosition: settingsModule.getWindowPosition(),
            alwaysOnTop: true /*this is to override the menubar default config*/
        }
    }

};