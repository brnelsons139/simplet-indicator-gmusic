'use strict';
module.exports = {
    labels: {
        show: 'Show',
        hide: '/Hide',
        playPause: 'Play/Pause',
        nextTrack: 'Next Track',
        previousTrack: 'Previous Track',
        settings: 'Settings',
        quit: 'Quit',
    },
    events: {
        closeSettings: 'closeSettings',
        getAppVersion: 'getAppVersion',
        setAppVersion: 'setAppVersion',
        getAppDataPath: 'getAppDataPath',
        setAppDataPath: 'setAppDataPath',
    }
};