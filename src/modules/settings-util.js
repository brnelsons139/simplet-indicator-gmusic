'use strict';
const ElectronSettings = require('electron-settings');
const path = require('path');
const preSettings = require('./settings').SETTINGS;
const PLAYER_SETTINGS = preSettings.PLAYER_SETTINGS.VALUES;
const WINDOW_SETTINGS = preSettings.WINDOW_SETTINGS.VALUES;
const HOT_KEY_SETTINGS = preSettings.HOT_KEYS.VALUES;
const MENU_SETTINGS = preSettings.MENU_SETTINGS.VALUES;

module.exports = (basePath) => {
    const module = {};

    const electronSettings = new ElectronSettings({
        'configDirPath': path.join(basePath, "GMusic", "userData"),
        'configFileName': 'userSettings'
    });

    function getDefault(setting) {
        electronSettings.set(setting.NAME, setting.DEFAULT_VAL);
        return setting.DEFAULT_VAL;
    }

    /**
     * Get a specified setting, must have field 'NAME'
     */
    module.get = (setting) => {
        return electronSettings.get(setting.NAME) || getDefault(setting);
    };

    /**
     * Get a specified setting, via String
     */
    module.getForUi = (settingName) => {
        return electronSettings.get(settingName) || this.getDefaultByName(settingName);
    };

    /**
     * Iterates all of the settings to find one that matches the requested name
     */
    module.getDefaultByName = (requestedName) => {
        const settingGroups = preSettings;
        for (const groupName in settingGroups) {
            if (settingGroups.hasOwnProperty(groupName)) {
                const groupSettings = settingGroups[groupName].VALUES;
                for (const settingName in groupSettings) {
                    if (groupSettings.hasOwnProperty(settingName)) {
                        const groupSetting = groupSettings[settingName];
                        if (groupSetting.NAME === requestedName)
                            return groupSetting.DEFAULT_VAL;
                    }
                }
            }
        }
        return null;
    };

    /**
     * Set a setting from the UI because it doesnt have access to the settings json
     * @param settingName String setting name to set
     * @param value String setting value to set
     */
    module.setFromUi = (settingName, value) => {
        electronSettings.set(settingName, value);
    };

    /**
     * Set setting from its NAME field, value is a String
     */
    module.set = (setting, value) => {
        electronSettings.set(setting.NAME, value)
    };

    /**
     * @returns music service setting, either Google Play, Spotify, or Pandora
     */
    module.getMusicService = () => {
        return module.get(PLAYER_SETTINGS.MUSIC_SERVICE);
    };

    /**
     * sets music service setting, either Google Play, Spotify, or Pandora
     */
    module.setMusicService = (musicService) => {
        module.set(PLAYER_SETTINGS.MUSIC_SERVICE, musicService);
    };

    module.getWindowPosition = () => {
        return module.get(WINDOW_SETTINGS.POSITION);
    };

    module.setWindowPosition = (position) => {
        module.set(WINDOW_SETTINGS.POSITION, position);
    };

    module.getWindowHeight = () => {
        return module.get(WINDOW_SETTINGS.WINDOW_HEIGHT);
    };

    module.setWindowHeight = (height) => {
        return module.set(WINDOW_SETTINGS.WINDOW_HEIGHT, height);
    };

    module.getWindowWidth = () => {
        return module.get(WINDOW_SETTINGS.WINDOW_WIDTH);
    };

    module.setWindowWidth = (width) => {
        return module.set(WINDOW_SETTINGS.WINDOW_WIDTH, width);
    };

    module.getTheme = () => {
        return module.get(PLAYER_SETTINGS.THEME);
    };

    module.setTheme = (newTheme) => {
        module.set(PLAYER_SETTINGS.THEME, newTheme);
    };

    module.getShowMenuPlayPause = () => {
        return module.get(MENU_SETTINGS.SHOW_PLAY_PAUSE) === "true";
    };

    module.setMenuShowPlayPause = (show) => {
        module.set(MENU_SETTINGS.SHOW_PLAY_PAUSE, show);
    };

    module.getShowMenuNext = () => {
        return module.get(MENU_SETTINGS.SHOW_NEXT_TRACK) === "true";
    };

    module.setMenuShowNext = (show) => {
        module.set(MENU_SETTINGS.SHOW_NEXT_TRACK, show);
    };

    module.getShowMenuPrevious = () => {
        return module.get(MENU_SETTINGS.SHOW_PREVIOUS_TRACK) === "true";
    };

    module.setMenuShowPrevious = (show) => {
        module.set(MENU_SETTINGS.SHOW_PREVIOUS_TRACK, show);
    };

    module.getShowHideWindowKey = () => {
        return module.get(HOT_KEY_SETTINGS.SHOW_HIDE_PLAYER) || module.getShowHideWindowKey();
    };

    module.setShowHideWindowKey = (newKey) => {
        module.set(HOT_KEY_SETTINGS.SHOW_HIDE_PLAYER, newKey);
    };

    module.getMediaNextKey = () => {
        return module.get(HOT_KEY_SETTINGS.MEDIA_NEXT) || module.getDefaultMediaNextKey();
    };

    module.setMediaNextKey = (newKey) => {
        module.set(HOT_KEY_SETTINGS.MEDIA_NEXT, newKey);
    };

    module.getMediaPreviousKey = () => {
        return module.get(HOT_KEY_SETTINGS.MEDIA_PREVIOUS) || module.getDefaultMediaPreviousKey();
    };

    module.setMediaPreviousKey = (newKey) => {
        module.set(HOT_KEY_SETTINGS.MEDIA_PREVIOUS, newKey);
    };

    module.getMediaPlayPauseKey = () => {
        return module.get(HOT_KEY_SETTINGS.MEDIA_PLAY_PAUSE) || module.getDefaultMediaPlayPauseKey();
    };

    module.setMediaPlayPauseKey = (newKey) => {
        module.set(HOT_KEY_SETTINGS.MEDIA_PLAY_PAUSE, newKey);
    };

    module.getAlwaysOnTop = () => {
        const alwaysOnTop = module.get(WINDOW_SETTINGS.ALWAYS_ON_TOP) || module.getDefaultAlwaysOnTop();
        return alwaysOnTop === "true";
    };

    module.setAlwaysOnTop = (newKey) => {
        module.set(WINDOW_SETTINGS.ALWAYS_ON_TOP, newKey);
    };

    module.getAutoHide = () => {
        const autoHide = module.get(WINDOW_SETTINGS.AUTO_HIDE) || module.getDefaultAutoHide();
        return autoHide === "true";
    };

    module.setAutoHide = (autoHide) => {
        this.set(WINDOW_SETTINGS.AUTO_HIDE, autoHide);
    };

    module.getShowMenuIcons = () => {
        const showIcons = module.get(MENU_SETTINGS.SHOW_MENU_ICONS) || module.getDefaultShowMenuIcons();
        return showIcons === "true";
    };

    module.setShowMenuIcons = (showIcons) => {
        module.set(MENU_SETTINGS.SHOW_MENU_ICONS, showIcons);
    };

    /**
     * Returns defined tray icon settings and theme
     */
    module.getTrayIconConfiguration = () => {
        const isWindows = process.platform === 'win32';
        const theme = module.getTheme();

        function getTrayIcon(fileType) {
            return path.join(__dirname, '../resources/images/' + 'gmusic-' + theme + '-indicator.' + fileType);
        }

        return isWindows ? getTrayIcon('ico') : getTrayIcon('png');
    };

    module.getDefaultAlwaysOnTop = () => {
        return getDefault(WINDOW_SETTINGS.ALWAYS_ON_TOP);
    };

    module.getDefaultAutoHide = () => {
        return getDefault(WINDOW_SETTINGS.AUTO_HIDE);
    };

    module.getDefaultShowMenuIcons = () => {
        return getDefault(MENU_SETTINGS.SHOW_MENU_ICONS);
    };

    module.getDefaultWindowHeight = () => {
        return getDefault(WINDOW_SETTINGS.WINDOW_HEIGHT);
    };

    module.getDefaultWindowWidth = () => {
        return getDefault(WINDOW_SETTINGS.WINDOW_WIDTH);
    };

    module.getDefaultWindowPosition = () => {
        return getDefault(WINDOW_SETTINGS.POSITION);
    };

    module.getDefaultMusicService = () => {
        return getDefault(PLAYER_SETTINGS.MUSIC_SERVICE);
    };

    module.getDefaultTheme = () => {
        return getDefault(PLAYER_SETTINGS.THEME);
    };

    module.getDefaultShowHidePlayerKey = () => {
        return getDefault(HOT_KEY_SETTINGS.SHOW_HIDE_PLAYER);
    };

    module.getDefaultMediaPlayPauseKey = () => {
        return getDefault(HOT_KEY_SETTINGS.MEDIA_PLAY_PAUSE);
    };

    module.getDefaultMediaNextKey = () => {
        return getDefault(HOT_KEY_SETTINGS.MEDIA_NEXT);
    };

    module.getDefaultMediaPreviousKey = () => {
        return getDefault(HOT_KEY_SETTINGS.MEDIA_PREVIOUS);
    };

    return module;
};